<script type='text/javascript'>
//<![CDATA[

  
  //The first part of the link without the date
var download = "https://scholar.google.at/scholar?hl=en&as_sdt=0%2C5&q=";

//Add var to extract url
var pathUrl = window.location.href;
  
//a step by step breakdown
function getImageDirectoryByFullURL(url){
    url = url.split('/'); //url = ["serverName","app",...,"bb65efd50ade4b3591dcf7f4c693042b"]
    url = url.pop();      //url = "bb65efd50ade4b3591dcf7f4c693042b"
    return url;           //return "bb65efd50ade4b3591dcf7f4c693042b"
}
var breakdownBit = getImageDirectoryByFullURL(pathUrl);
  
  //remove html in the end
  //ID = "100-200";
var values = breakdownBit.split('.htm');

var title_part1 = values[0];
var title_part2 = values[1];

  //attempting to lose the hyphens
var charRemoved =  title_part1.replace(/-/g, ' ');

  //Gets the link element in which the target URL will be changed
var link = document.getElementById("downloadLink");


  //Finally we change the links "href" attribute so it will now include the first part of the download link and date 
link.href = download + charRemoved;

//<![CDATA[
</script>