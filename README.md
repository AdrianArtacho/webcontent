# +WEB

This repository hosts webcontent in different formats, which can be pulled from the bitbucket repository to appear on a website (e.g. personla website). The big advantage is, the content can be edited locally, and pushed upstream.

### Including *.md* in wordpress

* [WebLinks](WebLinks.md)
* ...

---

### Converting *LaTeX* to *.md*

 For your need, first you need to [install pandoc into your system](http://pandoc.org/installing.html) (available for all MacOS, Windows, Linux) and then use this command line

```shell
pandoc -s example4.tex -o example5.md
```

You can also convert your texts online by using a tool provided [here](http://pandoc.org/try/).

### Combine complex LaTeX projects into one *.tex* file using *latexpand*

Asuming the meta LaTeX file (where all other are included) is called *MAIN.tex*:

1. Download the *source* .zip folder containing the LaTeX document.

2. Open a terminal in the folder where *MAIN.tex* lives

3. execute the perl command included in this repo:
   
   ```shell
   perl /../path/to/latexpand MAIN.tex > latexpanded.tex
   ```

4. done!