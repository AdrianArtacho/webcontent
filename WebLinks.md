### Research links [{}](https://trello.com/c/1v1evyy6/146-%F0%9F%93%96-dissertation-work-blog-papers-latex-etc)

- [Dissertation @ bitbucket](https://bitbucket.org/augmentedperformer/dissertation/src/master/) [{↔}](https://trello.com/c/Mu6jm6P6/148-adding-methods-to-dissertation-repository)

- [Dissertation @ Overleaf](https://www.overleaf.com/project/5bdb24678fcafe515c27b954)

- [Research Notes & Literature](http://emergendi.artacho.at/) → new [blog entry](https://www.blogger.com/blog/posts/8159905360940537380) [{→}](https://trello.com/c/9szwIvTX/2-reference-%E2%86%92-emergendi-blog)

### Music-making [{}](https://trello.com/c/DqYZOPuB/149-%F0%9F%8E%B5music-making)

* [Bra1nch1ld Lab (notes and ideas)](https://brainchild.artacho.at/) → new [blog entry](https://www.blogger.com/blog/posts/8316536274738949628)

* [Brainchild | Listen on hearthis.at](https://hearthis.at/brainchild/#tracks) → new [audio upload](https://hearthis.at/upload/)

### Misc

* Focus playlists ([spotify](https://open.spotify.com/playlist/37i9dQZF1DX7EF8wVxBVhG))
* [Workflow conventions](https://bitbucket.org/AdrianArtacho/workflow/src/master/README.md)
* [Octave online](https://octave-online.net/)

### Online presence

Self-publishing platform:

[https://www.yumpu.com/user/Adrian.Artacho](https://www.yumpu.com/user/Adrian.Artacho) (USER: *Adrian.Artacho*)

### ML and coding ressources:

There are some ML online ressources...

- [Magenta](https://magenta.tensorflow.org/)

- [Kaggle.com/learn](https://www.kaggle.com/learn) (by Google Magenta team)

- [Edge Impulse](https://studio.edgeimpulse.com/studio/163430/learning/keras-transfer-kws/5)

### [+Workflow](https://bitbucket.org/AdrianArtacho/workflow/src)
